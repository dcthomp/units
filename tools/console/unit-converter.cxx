#include "units/System.h"
#include "units/PreferredUnits.h"

#include <iostream>
#include <string>

template<typename Sys>
int convertOne(Sys& sys, const std::string& fromMeasurement, const std::string& toUnit)
{
  bool didParse;
  auto measurement = sys->measurement(fromMeasurement, &didParse);
  if (!didParse)
  {
    std::cerr << "Could not parse \"" << fromMeasurement << "\".\n";
    return 1;
  }
  auto unit = sys->unit(toUnit, &didParse);
  if (!didParse)
  {
    std::cerr << "Could not parse \"" << toUnit << "\".\n";
    return 1;
  }
  bool didConvert;
  auto result = sys->convert(measurement, unit, &didConvert);
  if (!didConvert)
  {
    std::cerr << "Could not convert " << measurement << " to " << unit << ".\n\n";
    return 1;
  }
  std::cout << result.m_value << " " << toUnit << "\n";
  return 0;
}

int main(int argc, char* argv[])
{
  auto sys = units::System::createWithDefaults();
  if (argc == 3)
  {
    return convertOne(sys, argv[1], argv[2]);
  }
  auto context = sys->activeContext();
  while (1)
  {
    std::cout << "You have: ";
    std::cout.flush();
    std::string source;
    std::getline(std::cin, source);
    if (source.empty())
    {
      break;
    }
    bool didParse;
    auto measurement = sys->measurement(source, &didParse);
    if (!didParse)
    {
      std::cerr << "Error: Could not parse \"" << source << "\".\n\n";
      continue;
    }
    std::cout << measurement.m_value << " [" << measurement.m_units.name() << "]"
      << " [[" << measurement.m_units.dimension().name().data() << "]]\n";
    if (context)
    {
      units::CompatibleUnitOptions options;
      auto compat = context->suggestedUnits(measurement.m_units.name().data(), options);
      if (!compat.empty())
      {
        std::cout << "Suggested alternatives:\n";
      }
      for (const auto& suggestion : compat)
      {
        std::cout << "    " << suggestion << "\n";
      }
    }


    std::cout << "You want: ";
    std::cout.flush();
    std::string target;
    std::getline(std::cin, target);

    auto unit = sys->unit(target, &didParse);
    if (!didParse)
    {
      std::cerr << "Error: Could not parse \"" << target << "\".\n\n";
      continue;
    }

    bool didConvert;
    auto result = sys->convert(measurement, unit, &didConvert);
    if (!didConvert)
    {
      std::cerr << "Could not convert " << measurement << " to " << unit << ".\n\n";
      continue;
    }

    std::cout << "\n  = " << result.m_value << " " << target << "\n";
  }

  return 0;
}
