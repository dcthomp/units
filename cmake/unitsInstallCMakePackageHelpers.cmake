function (_units_package_append_variables)
  set(_units_package_variables)
  foreach (var IN LISTS ARGN)
    if (NOT DEFINED "${var}")
      continue ()
    endif ()

    get_property(type_is_set CACHE "${var}"
      PROPERTY TYPE SET)
    if (type_is_set)
      get_property(type CACHE "${var}"
        PROPERTY TYPE)
    else ()
      set(type UNINITIALIZED)
    endif ()

    string(APPEND _units_package_variables
      # Only set the variable as a helper entry if there isn't already a value for it.
      "if (NOT DEFINED \"${var}\" OR NOT ${var})
  set(\"${var}\" \"${${var}}\" CACHE ${type} \"Third-party helper setting from \${CMAKE_FIND_PACKAGE_NAME}\")
endif ()
")
  endforeach ()

  set(units_find_package_code
    "${units_find_package_code}${_units_package_variables}"
    PARENT_SCOPE)
endfunction ()

set(_units_packages
  nlohmann_json
  pegtl
  # Qt5
)

set(units_find_package_code)
foreach (_units_package IN LISTS _units_packages)
  _units_package_append_variables(
    # Standard CMake `find_package` mechanisms.
    "${_units_package}_DIR"
    "${_units_package}_ROOT"

    # Per-package custom variables.
    ${${_units_package}_find_package_vars})
endforeach ()

file(GENERATE
  OUTPUT  "${units_cmake_build_dir}/units-find-package-helpers.cmake"
  CONTENT "${units_find_package_code}")
