#[=======================================================================[.rst:
units_encode_file
----------------

This function adds a custom command that generates a C++ header or Python
source-file ("<filename>") which encodes the contents of a file into a source
file that can be compiled.
By encoding the file into source code, there is no need to
search for it on the filesystem at run time; it is either compiled into a
library (C++) or included as part of a python module (which uses Python's
utilities for importing the data rather than requiring you to find it).

.. code-block:: cmake

   units_encode_file(
     <filename>
     FORMAT "py" (optional: defaults to "cpp" and C++; use "py" for python)
     NAME "var" (the name of the generated string literal or function)
     DESTINATION <outputFilename> (the output filename)
   )

Note that if you use ``units_encode_file()`` in a subdirectory relative to
the target which compiles its output, you must add the
value provided in ``DESTINATION`` to a custom_target (see `this FAQ`_ for details on why).

.. _this FAQ: https://gitlab.kitware.com/cmake/community/-/wikis/FAQ#how-can-i-add-a-dependency-to-a-source-file-which-is-generated-in-a-subdirectory

#]=======================================================================]
function(units_encode_file inFile)
  set(options "PYTHON;DEBUG")
  set(oneValueArgs "FORMAT;DESTINATION;NAME")
  cmake_parse_arguments(_UNIT_op "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
  if (_UNIT_op_UNPARSED_ARGUMENTS)
    message(FATAL_ERROR
      "Unrecognized arguments to units_encode_file: "
      "${_UNIT_op_UNPARSED_ARGUMENTS}")
  endif ()

  set(inFmt "cpp")
  if (_UNIT_op_PYTHON)
    set(inFmt "py")
  endif()
  if (DEFINED _UNIT_op_FORMAT)
    set(inFmt ${_UNIT_op_FORMAT})
  endif()

  # If asked to debug, pass that to the executable
  if (_UNIT_op_DEBUG)
    set(inDebug "DEBUG")
  endif()

  # If a literal/function name was provided, pass it along
  if (_UNIT_op_NAME)
    set(_nameArgs "NAME;${_UNIT_op_NAME}")
  else()
    message(FATAL_ERROR "You must provide a NAME identifier.")
  endif()

  if (_UNIT_op_DESTINATION)
    set(_genFile "${_UNIT_op_DESTINATION}")
  else()
    message(FATAL_ERROR "You must provide a DESTINATION filename.")
  endif()

  add_custom_command(
    OUTPUT  "${_genFile}"
    MAIN_DEPENDENCY "${inFile}"
    DEPENDS
      units_encode_file
      "${inFile}"
    COMMAND units_encode_file
    ARGS
      DESTINATION "${_genFile}"
      FORMAT "${inFmt}"
      ${_nameArgs}
      ${inDebug}
      "${inFile}"
  )
  set_source_files_properties(${_genFile} PROPERTIES GENERATED ON)

endfunction()
