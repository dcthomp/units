#include "units/grammar/Functions.h"
#include "units/string/Token.h"

#include "tao/pegtl/version.hpp"
#include "tao/pegtl/analyze.hpp"
#include "tao/pegtl/contrib/parse_tree.hpp"

namespace
{
// taken from vtk/Common/Core/vtkTypeName.h
std::string fixup(const std::string& name)
{
  std::string result = name;

  // we need to remove
  // MSVC-specific cruft from the symbol name.
#if defined(_MSC_VER)
  // MSVC returns a name with "class " or "struct " prepended. Remove it
  // for consistency with other platforms. Note that template parameters
  // also include "class " or "struct ", so we must search and replace
  // repeatedly.
  for (std::string::size_type pos = result.find("class "); pos != std::string::npos;
       pos = result.find("class ", pos + 1))
  {
    result = result.substr(0, pos) + result.substr(pos + 6);
  }
  for (std::string::size_type pos = result.find("struct "); pos != std::string::npos;
       pos = result.find("struct ", pos + 1))
  {
    result = result.substr(0, pos) + result.substr(pos + 7);
  }
  // MSVC reports anonymous namespaces like so: `anonymous namespace'
  // while others report them like so: (anonymous namespace). Fix it
  // to be consistent.
  for (std::string::size_type pos = result.find("`anonymous namespace'");
       pos != std::string::npos; pos = result.find("`anonymous namespace'", pos + 1))
  {
    result = result.substr(0, pos) + "(anonymous namespace)" + result.substr(pos + 21);
  }
  // MSVC does not include spaces after commas separating template
  // parameters. Add it in:
  for (std::string::size_type pos = result.find(','); pos != std::string::npos;
       pos = result.find(',', pos + 1))
  {
    result = result.substr(0, pos) + ", " + result.substr(pos + 1);
  }
#endif
  return result;
}
}

units_BEGIN_NAMESPACE
namespace grammar
{

using namespace string::literals;

// ----- dimension processing

template<typename NodeType>
void recurse_basic_dimension(NodeType* node, measurement_state& state)
{
  std::string text = node->content();
  ::units::string::Token token(text);
  auto it = state.system->m_dimensions.find(token);
  if (it == state.system->m_dimensions.end())
  {
    throw tao::pegtl::parse_error("Unknown dimension \"" + text + "\".", node->begin());
  }
  state.dimensions = Dimension({*it->second, 1});
}

template<typename NodeType>
void recurse_power_dimension(NodeType* node, measurement_state& state)
{
  measurement_state tmp;
  tmp.system = state.system;
  tmp.parsing_dimensions = true;
  recurse_basic_dimension(node->children.front().get(), tmp);
  if (node->children.size() == 1)
  {
    state.dimensions = tmp.dimensions;
    return;
  }
  // TODO: This ignores the work done by capture_exponent…
  double exponent = strtod(node->children.back()->content().c_str(), nullptr);
  state.dimensions = tmp.dimensions.pow(exponent);
}

template<typename NodeType>
void recurse_product_dimension(NodeType* node, measurement_state& state)
{
  measurement_state tmp;
  tmp.system = state.system;
  tmp.parsing_dimensions = true;
  bool numer = true;
  for (const auto& child : node->children)
  {
    ::units::string::Token childType(fixup(child->name()));
    switch (childType.id())
    {
    case "units::grammar::divide_kw"_hash:
      numer = false;
      break;
    case "units::grammar::multiply_kw"_hash:
      numer = true;
      break;
    case "units::grammar::lparen_kw"_hash:
    case "units::grammar::rparen_kw"_hash:
      // Ignore these; they must always be at the
      // begin (respectively, end) of the children:
      // assert(child == node->children.front() || child == node->children.back())
      // They occur because we evaluate a parenthetical expression below
      // as if it were a sequence of product expressions (which it is).
      break;
    case "units::grammar::paren_expression"_hash:
      tmp.dimensions = Dimension();
      recurse_product_dimension(child.get(), tmp);
      state.dimensions = state.dimensions * (numer ? tmp.dimensions : tmp.dimensions.pow(-1));
      break;
    case "units::grammar::power_expression"_hash:
      tmp.dimensions = Dimension();
      recurse_power_dimension(child.get(), tmp);
      // TODO: Handle prefixes/values properly?
      state.dimensions = state.dimensions * (numer ? tmp.dimensions : tmp.dimensions.pow(-1));
      break;
    case "units::grammar::product_expression"_hash:
      tmp.dimensions = Dimension();
      recurse_product_dimension(child.get(), tmp);
      // TODO: Handle prefixes/values properly?
      state.dimensions = state.dimensions * (numer ? tmp.dimensions : tmp.dimensions.pow(-1));
      break;
    default:
    case "units::grammar::foo"_hash:
      std::cerr << "Unsupported product child node " << child->name() << ".";
      break;
    }
  }
}

template<typename NodeType>
void recurse_shift_dimension(NodeType* node, measurement_state& state)
{
  if (node->children.size() != 1)
  {
    throw tao::pegtl::parse_error("Dimension expressions cannot apply reference shifts.", node->begin());
  }
  recurse_product_dimension(node->children.front().get(), state);
}

template<typename NodeType>
void recurse_tree_dimension(NodeType* node, measurement_state& state)
{
  if (!node) { return; }
  ::units::string::Token nodeType(fixup(node->name()));
  switch (nodeType.id())
  {
  case "units::grammar::product_expression"_hash:
    // children should be multiplied together, paying attention to any divide_kw tokens.
    recurse_product_dimension(node, state);
    break;
  case "units::grammar::power_expression"_hash:
    // child is a unit raised to a power.
    recurse_power_dimension(node, state);
    break;
  case "units::grammar::shifted_expression"_hash:
    // child is either a subexpression or a shift expression.
    // If it is a shift expression, there will be multiple children.
    if (node->children.size() == 1)
    {
      recurse_tree_dimension(node->children.front().get(), state);
    }
    else
    {
      recurse_shift_dimension(node, state);
    }
    break;
  case "units::grammar::basic_expression"_hash:
    // child is a single unit.
    recurse_basic_dimension(node, state);
    break;
  }
}

// ----- unit processing

template<typename NodeType>
void recurse_basic(NodeType* node, measurement_state& state)
{
  std::string text = node->content();
  ::units::string::Token token(text);
  Prefix prefix;
  auto it = state.system->m_unitsByName.find(token);
  if (it == state.system->m_unitsByName.end())
  {
    bool found = false;
    // No unit-only match. See if we can find a prefix matching the start of the string.
    for (const auto& prefixEntry : state.system->m_prefixes)
    {
      std::string pfx = prefixEntry.first.data();
      string::Token testPrefix = text.substr(0, pfx.size());
      if (testPrefix == prefixEntry.first)
      {
        it = state.system->m_unitsByName.find(text.substr(pfx.size(), std::string::npos));
        if (it != state.system->m_unitsByName.end())
        {
          found = true;
          prefix = *prefixEntry.second;
        }
      }
    }
    if (!found)
    {
      throw tao::pegtl::parse_error("Unknown unit \"" + text + "\".", node->begin());
    }
  }
  Unit unit({*it->second, 1});
  if (prefix.m_prefix)
  {
    unit = prefix * unit;
  }
  state.result = state.result * unit;
}

template<typename NodeType>
void recurse_power(NodeType* node, measurement_state& state)
{
  measurement_state tmp;
  tmp.system = state.system;
  recurse_basic(node->children.front().get(), tmp);
  if (node->children.size() == 1)
  {
    state.result = tmp.result;
    return;
  }
  // TODO: This ignores the work done by capture_exponent…
  double exponent = strtod(node->children.back()->content().c_str(), nullptr);
  state.result = tmp.result.pow(exponent);
}

template<typename NodeType>
void recurse_value(NodeType* node, measurement_state& state)
{
  double val = strtod(node->content().c_str(), nullptr);
  if (state.have_value)
  {
    std::cerr << "Sequences of values not supported yet. Ignoring " << node->content() << ".\n";
    return;
  }
  state.value = val;
  state.have_value = true;
}

template<typename NodeType>
void recurse_product(NodeType* node, measurement_state& state)
{
  measurement_state tmp;
  tmp.system = state.system;
  bool numer = true;
  for (const auto& child : node->children)
  {
    ::units::string::Token childType(fixup(child->name()));
    switch (childType.id())
    {
    case "units::grammar::divide_kw"_hash:
      numer = false;
      break;
    case "units::grammar::multiply_kw"_hash:
      numer = true;
      break;
    case "units::grammar::lparen_kw"_hash:
    case "units::grammar::rparen_kw"_hash:
      // Ignore these; they must always be at the
      // begin (respectively, end) of the children:
      // assert(child == node->children.front() || child == node->children.back())
      // They occur because we evaluate a parenthetical expression below
      // as if it were a sequence of product expressions (which it is).
      break;
    case "units::grammar::paren_expression"_hash:
      tmp.result = Unit();
      recurse_product(child.get(), tmp);
      // TODO: Handle prefixes/values properly?
      state.value = state.value * (numer ? tmp.value : 1.0 / tmp.value);
      state.result = state.result * (numer ? tmp.result : tmp.result.pow(-1));
      break;
    case "units::grammar::power_expression"_hash:
      tmp.result = Unit();
      recurse_power(child.get(), tmp);
      // TODO: Handle prefixes/values properly?
      state.value = state.value * (numer ? tmp.value : 1.0 / tmp.value);
      state.result = state.result * (numer ? tmp.result : tmp.result.pow(-1));
      break;
    case "units::grammar::product_expression"_hash:
      tmp.result = Unit();
      recurse_product(child.get(), tmp);
      // TODO: Handle prefixes/values properly?
      state.value = state.value * (numer ? tmp.value : 1.0 / tmp.value);
      state.result = state.result * (numer ? tmp.result : tmp.result.pow(-1));
      break;
    default:
    case "units::grammar::foo"_hash:
      std::cerr << "Unsupported product child node " << child->name() << ".";
      break;
    }
  }
}

template<typename NodeType>
void recurse_shift(NodeType* node, measurement_state& state)
{
  measurement_state tmp;
  tmp.system = state.system;
  recurse_product(node->children.front().get(), tmp);
  if (node->children.size() == 1)
  {
    state.result = tmp.result;
    return;
  }
  double shift = strtod(node->children.back()->content().c_str(), nullptr);
  // TODO: FIXME: Need to figure out how to handle shifts.
  state.result = tmp.result;
}

template<typename NodeType>
void recurse_tree(NodeType* node, measurement_state& state)
{
  if (!node) { return; }
  ::units::string::Token nodeType(fixup(node->name()));
  switch (nodeType.id())
  {
  case "units::grammar::value"_hash:
    recurse_value(node, state);
    break;
  case "units::grammar::product_expression"_hash:
    // children should be multiplied together, paying attention to any divide_kw tokens.
    recurse_product(node, state);
    break;
  case "units::grammar::power_expression"_hash:
    // child is a unit raised to a power.
    recurse_power(node, state);
    break;
  case "units::grammar::shifted_expression"_hash:
    // child is either a subexpression or a shift expression.
    // If it is a shift expression, there will be multiple children.
    if (node->children.size() == 1)
    {
      recurse_tree(node->children.front().get(), state);
    }
    else
    {
      recurse_shift(node, state);
    }
    break;
  case "units::grammar::basic_expression"_hash:
    // child is a single unit.
    recurse_basic(node, state);
    break;
  }
}

template<typename NodeType, typename NodeValueMap>
void recurse_measurement_tree(NodeType* node, measurement_state& state, NodeType* parent, NodeValueMap& nodeValues)
{
  if (!node) { return; }
  // Depth-first descend over all children before evaluating this node:
  for (const auto& child : node->children)
  {
    recurse_measurement_tree(child.get(), state, node, nodeValues);
  }
  ::units::string::Token nodeType(fixup(node->name()));
  switch (nodeType.id())
  {
    // These nodes just copy their lone child's value to themselves.
  case "units::grammar::unit_parenthetical"_hash:
  case "units::grammar::atomic"_hash:
    nodeValues[node] = nodeValues[node->children.front().get()];
    break;


    // This node type holds a floating-point number and no units.
  case "units::grammar::value"_hash:
    nodeValues[node].m_value = strtod(node->content().c_str(), nullptr);
    // state.value = strtod(node->content().c_str(), nullptr);
    // state.have_value = true;
    break;


    // Set this node to the unit or unit+prefix identified by the content.
  case "units::grammar::unit_kw"_hash:
    nodeValues[node].m_units = state.system->evaluateUnitPrefixToken(node->content());
    nodeValues[node].m_value = 1.0;
    break;

    // These are all handled by their parent nodes as they iterate over children.
    // They take on no value themselves; rather they describe how sibling values are accumulated.
  case "units::grammar::exponent"_hash:
  case "units::grammar::exponent_kw"_hash:
  case "units::grammar::unit_exponential"_hash:
  case "units::grammar::infix_operator"_hash:
  case "units::grammar::unit_infix_op"_hash:
  case "units::grammar::unit_infix_separator"_hash:
  case "units::grammar::unit_combined_separator"_hash:
  case "units::grammar::unit_exponential_separator"_hash:
    break;


    // An atomic measurement always has exactly 2 nodes: a number node and a units node.
  case "units::grammar::measurement_atomic"_hash:
    nodeValues[node].m_value = nodeValues[node->children.front().get()].m_value;
    nodeValues[node].m_units = nodeValues[node->children.back().get()].m_units;
    break;

    // These nodes are parents to sequences of children with values and operands.
  case "units::grammar::measurement_grammar"_hash:
  case "units::grammar::expression"_hash:
  case "units::grammar::unit_expression"_hash:
  case "units::grammar::unit_grammar"_hash:
    {
      // We do two passes through our children:
      // + one to compute the value+unit of each node that can hold a value+unit
      //   and to apply exponents to their left sibling's value+unit; and
      // + one to compute the product (adjusted for division operands) of all children with a value+unit.
      for (std::size_t ii = 0; ii < node->children.size(); ++ii)
      {
        auto* child = node->children[ii].get();
        auto childName = ::units::string::Token(fixup(child->name()));
        if (childName.id() == "units::grammar::unit_exponential"_hash ||
          childName.id() == "units::grammar::unit_exponential_separator"_hash)
        {
          std::string expStr = child->content().substr(
            child->content()[0] == '^' ? 1 : 2, std::string::npos);
          double expVal = std::strtod(expStr.c_str(), nullptr);
          for (std::size_t jj = ii - 1; jj >= 0; --jj)
          {
            auto it = nodeValues.find(node->children[jj].get());
            if (it != nodeValues.end())
            {
              it->second = it->second.pow(expVal);
              break;
            }
          }
        }
      }
      // Now, any child node in the map that has a value should be processed in order.
      // Every child should have the same precedence, so we just go left to right.
      double factor = +1.0; // multiplier for numerator/denominator flipping
      char operand = '*'; // multiply by default
      if (nodeValues.find(node) == nodeValues.end())
      {
        nodeValues[node] = Measurement(1.0, Unit());
      }
      auto& nodeValue(nodeValues[node]);
      for (std::size_t ii = 0; ii < node->children.size(); ++ii)
      {
        auto it = nodeValues.find(node->children[ii].get());
        if (it != nodeValues.end())
        {
          switch (operand)
          {
          case '*':
            nodeValue *= it->second.pow(factor);
            break;
          case '+':
            nodeValue = nodeValue + it->second;
            break;
          case '-':
            nodeValue = nodeValue - it->second;
          }
        }
        else
        {
          auto* child = node->children[ii].get();
          auto childName = ::units::string::Token(fixup(child->name()));
          switch (childName.id())
          {
          case "units::grammar::unit_infix_op"_hash:
          case "units::grammar::unit_infix_separator"_hash:
          case "units::grammar::infix_operator"_hash:
            if (child->content() == "/" || child->content() == "÷")
            {
              factor = -1.0;
              operand = '*';
            }
            else if (child->content() == "*" || child->content() == "×" || child->content() == "·")
            {
              operand = '*';
            }
            else if (child->content() == "+")
            {
              operand = '+';
            }
            else if (child->content() == "-")
            {
              operand = '-';
            }
            break;
          default:
            break;
          }
        }
      }
    }
    break;


  default:
    std::cerr << "ERROR: Encountered unsupported node type \"" << nodeType.data() << "\"\n";
    state.have_value = false;
  }
}

template<typename NodeType>
void print_tree(NodeType* node)
{
  if (!node) { return; }
  if (node->has_content())
  {
    std::cout << "  n" << node << " [label=\"" << node->name() << ", text " << node->content() << "\"]\n";
  }
  else
  {
    std::cout << "  n" << node << "\n";
  }
  for (const auto& child : node->children)
  {
    print_tree(child.get());
  }
  for (const auto& child : node->children)
  {
    std::cout << "  n" << node << " -> n" << child.get() << "\n";
  }
}

#if 0
template<typename Rule>
using unit_selector = tao::pegtl::parse_tree::selector<
  Rule,
  tao::pegtl::parse_tree::store_content::on<
    divide_kw,
    shift_kw,
    exponent_kw,
    power_expression,
    basic_expression,
    value
  >
>;

#else
template<typename Rule> struct unit_selector : std::false_type {};
template<> struct unit_selector<divide_kw> : std::true_type {};
template<> struct unit_selector<shift_kw> : std::true_type {};
template<> struct unit_selector<multiply_kw> : std::true_type {};
template<> struct unit_selector<lparen_kw> : std::true_type {};
template<> struct unit_selector<rparen_kw> : std::true_type {};
template<> struct unit_selector<exponent_kw> : std::true_type {};
template<> struct unit_selector<exponent> : std::true_type {};
template<> struct unit_selector<shifted_expression> : std::true_type {};
template<> struct unit_selector<product_expression> : std::true_type {};
template<> struct unit_selector<paren_expression> : std::true_type {};
template<> struct unit_selector<power_expression> : std::true_type {};
template<> struct unit_selector<basic_expression> : std::true_type {};
template<> struct unit_selector<value> : std::true_type {};

template<typename Rule> struct measurement_grammar_selector : std::false_type {};
template<> struct measurement_grammar_selector<unit_expression> : std::true_type {};
template<> struct measurement_grammar_selector<unit_infix_op> : std::true_type {};
template<> struct measurement_grammar_selector<unit_parenthetical> : std::true_type {};
// template<> struct measurement_grammar_selector<unit_combined_separator> : std::true_type {};
template<> struct measurement_grammar_selector<unit_exponential> : std::true_type {};
template<> struct measurement_grammar_selector<unit_exponential_separator> : std::true_type {};
template<> struct measurement_grammar_selector<unit_infix_separator> : std::true_type {};
template<> struct measurement_grammar_selector<unit_kw> : std::true_type {};
// template<> struct measurement_grammar_selector<exponent_kw> : std::true_type {};
// template<> struct measurement_grammar_selector<exponent> : std::true_type {};
template<> struct measurement_grammar_selector<infix_operator> : std::true_type {};
template<> struct measurement_grammar_selector<parenthetical_atomic> : std::true_type {};
template<> struct measurement_grammar_selector<measurement_atomic> : std::true_type {};
template<> struct measurement_grammar_selector<atomic> : std::true_type {};
template<> struct measurement_grammar_selector<expression> : std::true_type {};
template<> struct measurement_grammar_selector<measurement_grammar> : std::true_type {};
template<> struct measurement_grammar_selector<value> : std::true_type {};
template<> struct measurement_grammar_selector<unit_grammar> : std::true_type {};
#endif

bool parseUnits(const std::string& input, measurement_state& state)
{
  bool ok = true;
  tao::pegtl::string_input<> in(input, "parseUnits");
  try
  {
    std::unique_ptr<tao::pegtl::parse_tree::node> forest;
    if (state.parsing_measurement)
    {
      forest = tao::pegtl::parse_tree::parse<measurement, unit_selector>(in);
    }
    else
    {
      forest = tao::pegtl::parse_tree::parse<units, unit_selector>(in);
    }
    state.result = Unit();
    // std::cout << "digraph {\n";
    for (const auto& tree : forest->children)
    {
      // print_tree(tree.get());
      if (state.parsing_dimensions)
      {
        recurse_tree_dimension(tree.get(), state);
      }
      else
      {
        recurse_tree(tree.get(), state);
      }
    }
    // std::cout << "}\n";
  }
  catch (tao::pegtl::parse_error& err)
  {
    const auto p = err.positions.front();
    if (state.m_debug)
    {
      std::cerr << err.what() << "\n"
#if TAO_PEGTL_VERSION_MAJOR <= 2 && TAO_PEGTL_VERSION_MINOR <= 7
        << in.line_as_string(p) << "\n"
#else
        << in.line_at(p) << "\n"
#endif
        << std::string(p.byte_in_line, ' ') << "^\n";
    }
    ok = false;
    return ok;
  }

  return ok;
}

#if 0
// As an alternative to recurse_measurement_tree, we could use a
// cusom node type like this to process parse nodes as they are
// created. It would be nice to do this since the failure() template
// could record the hierarchy of failures as they are unrolled
// and provide good insight into what bytes failed to match what rules.
// But it is a lot of work.
struct measurement_node : tao::pegtl::parse_tree::basic_node<measurement_node>
{
  using superclass = tao::pegtl::parse_tree::basic_node<measurement_node>;

  measurement_node() = default;
  measurement_node(const measurement_node&) = delete;
  measurement_node(measurement_node&&) = delete;
  void operator = (const measurement_node&) = delete;
  void operator = (measurement_node&&) = delete;

  ~measurement_node() = default;

  /// Called when an attempt to parse a rule begins.
  template<typename Rule, typename Input, typename... States>
  using superclass::start<Rule, Input, States...>;

  /// Called when a rule match succeeds.
  template< typename Rule, typename Input, typename... States >
  void success(const Input& in, States&&... st)
  {
    superclass::success(in, st...);
    // Now use Rule to configure local variables.
  }

  /// Called when a rule match fails after start() was called.
  template<typename Rule, typename Input, typename... States>
  using superclass::failure<Rule, Input, States...>;

  /// Called as children are added (after success).
  template< typename... States >
  void emplace_back( std::unique_ptr< node_t > child, States&&... st )
  {
    superclass::emplace_back(child, st...);
    // Now use child (es. child->id) to configure local variables.
    // Mainly this is used to propagate measurements up the parse-tree.
  }
};
#endif // 0

bool parseUnits2(const std::string& input, measurement_state& state)
{
  bool ok = true;
  tao::pegtl::string_input<> in(input, "parseUnits2");
  try
  {
    std::unique_ptr<tao::pegtl::parse_tree::node> forest;
    operators ops;
    stacks stacks;
    if (state.parsing_measurement)
    {
      forest = tao::pegtl::parse_tree::parse<measurement_grammar, measurement_grammar_selector>(in, ops, stacks, state.system.get());
    }
    else if (!state.parsing_dimensions)
    {
      forest = tao::pegtl::parse_tree::parse<unit_grammar, measurement_grammar_selector>(in, ops, stacks, state.system.get());
    }
    state.result = Unit();
    std::map<tao::pegtl::parse_tree::node*, ::units::Measurement> nodeValues;
    if (state.m_debug) { std::cout << "digraph {\n"; }
    for (const auto& tree : forest->children)
    {
      if (state.m_debug) { print_tree(tree.get()); }
      recurse_measurement_tree(tree.get(), state, (decltype(tree.get()))nullptr, nodeValues);
#if 0
      if (state.m_debug)
      {
        std::cout << "### Node value summary ###\n";
        for (const auto& entry : nodeValues)
        {
          std::cout << entry.first << ": " << entry.first->name() << " " << entry.first->content() << ": " << entry.second << "\n";
        }
        std::cout << "### Node value summary\n";
      }
#endif
      state.value = nodeValues[tree.get()].m_value;
      state.result = nodeValues[tree.get()].m_units;
#if 0
      if (state.parsing_dimensions)
      {
        recurse_tree_dimension(tree.get(), state);
      }
      else
      {
        recurse_tree(tree.get(), state);
      }
#endif
    }
    if (state.m_debug) { std::cout << "}\n"; }
  }
  catch (tao::pegtl::parse_error& err)
  {
    const auto p = err.positions.front();
    if (state.m_debug)
    {
      std::cerr << err.what() << "\n"
#if TAO_PEGTL_VERSION_MAJOR <= 2 && TAO_PEGTL_VERSION_MINOR <= 7
        << in.line_as_string(p) << "\n"
#else
        << in.line_at(p) << "\n"
#endif
        << std::string(p.byte_in_line, ' ') << "^\n";
    }
    ok = false;
    return ok;
  }

  return ok;
}

} // namespace grammar
units_CLOSE_NAMESPACE
