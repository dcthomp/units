#include <algorithm>
#include <fstream>
#include <iostream>
#include <locale> // for std::isalpha, std::isdigit
#include <regex>
#include <sstream>
#include <string>

static bool debug = false;
static std::size_t pieceLen = 16383; // = 2^14 - 1

int usage(const std::string& message, int code)
{
  std::cout << R"(Usage:
    units_encode_file [FORMAT <format>] [PIECE_SIZE <size>]
      [DESTINATION <output>] [DEBUG] NAME <name> <input>
where:
    <format>  is one of "py" or "cpp" (the default is "cpp").
    <name>    is a valid C/C++ identifier to use as the variable
              name or function name that produces the contents of
              the encoded file.
    <size>    is the number of bytes allowed per string literal.
    <output>  is the path to the output file to generate.
    <input>   is the path to the file to encode.

Notes:
    1. If DESTINATION is specified, the given output filename is used.
       Otherwise, the stem of the input file plus any type and extension
       are combined. For example, an input `/path/to/foo.xml`, a TYPE
       `xml`, and an EXT `h` will produce `foo_xml.h` as output.
    2. NAME is required. It is used as the variable or function name.
       Note that whatever name is used will be sanitized to produce
       a valid C/C++ identifier by omitting invalid characters.
       If no valid characters are present, an underscore (`_`) is used.
    4. The default <size> is 16383.
    4. If DEBUG is specified, then informational messages will be
       printed to the terminal.

)" << message << "\n";

  return code;
}

inline bool isStart(char xx)
{
  return std::isalpha(xx) || xx == '_';
}

inline bool isContinue(char xx)
{
  return std::isdigit(xx) || isStart(xx);
}

std::string sanitizeIdentifier(const std::string& identifier)
{
  bool first = true;
  std::ostringstream result;
  for (const auto& character : identifier)
  {
    if ((first && isStart(character)) || (!first && isContinue(character)))
    {
      result << character;
      first = false;
    }
  }
  if (result.str().empty())
  {
    result << "_";
  }
  return result.str();
}

bool isKeyword(const std::string& word)
{
  return word == "FORMAT" || word == "NAME" ||
    word == "PIECE_SIZE" || word == "DESTINATION" || word == "DEBUG";
}

bool readFile(std::string& contents, const std::string& inputFilename)
{
  std::ifstream file(inputFilename.c_str());
  if (!file.good())
  {
    return false;
  }
  contents =
    std::string((std::istreambuf_iterator<char>(file)), (std::istreambuf_iterator<char>()));
  return true;
}

bool writeFile(const std::string& contents, const std::string& outputFilename)
{
  std::ofstream file(outputFilename.c_str());
  if (!file.good())
  {
    return false;
  }
  file << contents;
  file.close();
  return true;
}

std::string encodeAsPython(const std::string& name, const std::string& contents)
{
  return name + R"( = """)" + contents + R"(""")";
}

std::string encodeAsCppFunction(const std::string& name, const std::string& contents)
{
  std::ostringstream encoded;
  encoded << R"(
#include <mutex>
#include <string>
#include <thread>

namespace
{

inline const std::string& )"
          << name << R"(()
{
  static std::mutex lock;
  static std::string data;
  std::lock_guard<std::mutex> guard(lock);
  if (data.empty())
  {
)";

  std::size_t dataLen = contents.size();
  // constexpr std::size_t pieceLen = 2048; // = 2^11
  std::size_t numPieces = dataLen / pieceLen;
  for (std::size_t ii = 0; ii < numPieces; ++ii)
  {
    encoded << "    data += R\"v0g0nPoetry(" << contents.substr(ii * pieceLen, pieceLen)
            << ")v0g0nPoetry\";\n";
  }
  // If there is a remainder smaller than the buffer size, encode it.
  if (dataLen % pieceLen)
  {
    encoded << "    data += R\"v0g0nPoetry("
            << contents.substr(numPieces * pieceLen) // , std::string::npos)
            << ")v0g0nPoetry\";\n";
  }

  encoded << R"(
  }
  return data;
}
} // anonymous namespace)";

  return encoded.str();
}

int main(int argc, char* argv[])
{
  enum EncodeTo
  {
    Python,
    CppFunction
  };
  std::vector<std::string> includeDirs;
  EncodeTo encodeTo = EncodeTo::CppFunction;
  std::string inputFilename;
  std::string outputFilename;
  std::string ext;
  std::string encType;
  std::string nameToken;
  for (int ii = 1; ii < argc; ++ii)
  {
    std::string argii = argv[ii];
    if (argii == "FORMAT")
    {
      if (ii + 1 >= argc)
      {
        return usage("The TYPE keyword requires a value after it.", 1);
      }
      encType = argv[ii + 1];
      ++ii;
      std::transform(encType.begin(), encType.end(), encType.begin(), ::tolower);
      if (encType.substr(0, 2) == "py")
      {
        encodeTo = EncodeTo::Python;
      }
      else if (encType == "c++" || encType == "cpp")
      {
        encodeTo = EncodeTo::CppFunction;
      }
      else
      {
        return usage("Unknown encoding type " + encType, 1);
      }
    }
    else if (argii == "DESTINATION")
    {
      if (ii + 1 >= argc)
      {
        return usage("The DESTINATION keyword requires a value after it.", 1);
      }
      outputFilename = argv[ii + 1];
      ++ii;
    }
    else if (argii == "NAME")
    {
      if (ii + 1 >= argc)
      {
        return usage("The NAME keyword requires a value after it.", 1);
      }
      nameToken = sanitizeIdentifier(argv[ii + 1]);
      ++ii;
    }
    else if (argii == "PIECE_SIZE")
    {
      if (ii + 1 >= argc)
      {
        return usage("The NAME keyword requires a value after it.", 1);
      }
      pieceLen = atoi(argv[ii + 1]);
      ++ii;
    }
    else if (argii == "DEBUG")
    {
      debug = true;
    }
    else if (argii == "--help" || argii == "-h")
    {
      return usage("", 0);
    }
    else
    {
      if (inputFilename.empty())
      {
        inputFilename = argii;
      }
      else
      {
        return usage("Only one input file at a time is allowed.", 1);
      }
    }
  }
  if (inputFilename.empty())
  {
    return usage("You must provide an input filename.", 1);
  }
  if (ext.empty())
  {
    ext = (encodeTo == EncodeTo::Python ? "py" : "h");
  }
  if (outputFilename.empty())
  {
    outputFilename = inputFilename + "_" + encType + "." + ext;
  }
  if (nameToken.empty())
  {
    // TODO: sanitize stem to a C variable name.
    return usage("You must specify a function/variable name.", 1);
  }
  if (debug)
  {
    std::cout << "Encode \"" << inputFilename << "\" to \"" << outputFilename << "\""
              << " as " << nameToken
              << (encodeTo == EncodeTo::CppFunction ? "()" : " python")
              << "\n";
  }

  std::string contents;
  if (!readFile(contents, inputFilename))
  {
    return usage("ERROR: Unable to read file.", 3);
  }
  switch (encodeTo)
  {
    case EncodeTo::Python:
      contents = encodeAsPython(nameToken, contents);
      break;
    default: // fall through
    case EncodeTo::CppFunction:
      contents = encodeAsCppFunction(nameToken, contents);
      break;
  }
  if (!writeFile(contents, outputFilename))
  {
    return usage("ERROR: Unable to write file \"" + outputFilename + "\"", 9);
  }
  return 0;
}
