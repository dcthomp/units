#include "units/json/Helper.h"

#include "units/System.h"

#include <list>
#include <mutex>

namespace
{

static thread_local std::list<units::json::Helper> t_helpers;

} // anonymous namespace

units_BEGIN_NAMESPACE
namespace json
{

Helper& Helper::instance()
{
  if (t_helpers.empty())
  {
    t_helpers.emplace_back(std::make_shared<System>());
  }
  return t_helpers.back();
}

Helper& Helper::pushInstance(const std::shared_ptr<System>& system)
{
  t_helpers.emplace_back(system);
  return t_helpers.back();
}

bool Helper::popInstance()
{
  if (t_helpers.empty())
  {
    return false;
  }
  t_helpers.pop_back();
  return true;
}

} // namespace json
units_CLOSE_NAMESPACE
