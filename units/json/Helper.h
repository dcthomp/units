#ifndef units_json_Helper_h
#define units_json_Helper_h

#include "units/Exports.h"
#include "units/Options.h"

#include <memory> // for std::shared_ptr<T>

units_BEGIN_NAMESPACE
struct System;

namespace json
{

struct UNITS_EXPORT Helper
{
  static Helper& instance();
  static Helper& pushInstance(const std::shared_ptr<System>& helper);
  static bool popInstance();

  Helper(const std::shared_ptr<System>& system)
    : m_system(system)
  {
  }
  virtual ~Helper() = default;

  std::shared_ptr<System> system() const { return m_system; }

  std::shared_ptr<System> m_system;
};

} // namespace json
units_CLOSE_NAMESPACE

#endif // units_json_Helper_h
