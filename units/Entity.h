#ifndef units_Entity_h
#define units_Entity_h

#include "units/string/Token.h"

#include <memory> // for std::enable_shared_from_this<>
#include <unordered_set>

units_BEGIN_NAMESPACE

/// A base holding names.
struct UNITS_EXPORT Entity : public std::enable_shared_from_this<Entity>
{
  Entity() = default;
  Entity(units::string::Token symbol, units::string::Token name, units::string::Token description)
    : m_symbol(symbol)
    , m_name(name)
    , m_description(description)
  {
  }
  Entity(units::string::Token symbol, units::string::Token name, units::string::Token plural, units::string::Token description)
    : m_symbol(symbol)
    , m_name(name)
    , m_plural(plural)
    , m_description(description)
  {
  }

  /// Add an alternate symbol-alias to this entity.
  ///
  /// This may be used for lookup in contexts where only
  /// symbols are allowed or in contexts where names and
  /// symbols are allowed.
  bool addSymbolAlias(units::string::Token alias)
  {
    return m_symbolAliases.insert(alias).second;
  }

  /// Add an alternate name-alias to this entity.
  ///
  /// This may be used for lookup in contexts where only
  /// names are allowed or in contexts where names and
  /// symbols are allowed.
  bool addNameAlias(units::string::Token alias)
  {
    return m_nameAliases.insert(alias).second;
  }

  /// The primary abbreviation for this entity.
  units::string::Token m_symbol;
  /// The primary "long name" for this entity.
  units::string::Token m_name;
  /// The plural form of the long name of this entity.
  units::string::Token m_plural;
  /// True when the plural and singular long name are identical.
  bool m_noPlural{ false };
  /// Secondary abbreviations for this entity.
  std::unordered_set<units::string::Token> m_symbolAliases;
  /// Secondary long names for this entity.
  std::unordered_set<units::string::Token> m_nameAliases;
  /// A user-presentable (XHTML) description of this entity.
  units::string::Token m_description;
};

units_CLOSE_NAMESPACE

#endif // units_Entity_h
