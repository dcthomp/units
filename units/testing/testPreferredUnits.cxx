#include "units/PreferredUnits.h"
#include "units/System.h"

#include "units/json/jsonUnits.h"

#include <iostream>

using namespace units;

int testPreferredUnits(int argc, char* argv[])
{
  (void)argc;
  (void)argv;
  bool ok = true;

  auto sys = System::createWithDefaults();

  if (sys->m_unitContexts.size() != 1)
  {
    std::cerr
      << "ERROR: default system should have 1 unit context, "
      << "found " << sys->m_unitContexts.size() << ".\n";
    ok = false;
  }

  auto ctxt1 = PreferredUnits::create(sys.get(), true);
  if (!ctxt1 || !ctxt1->m_unitsByDimension.empty())
  {
    std::cerr << "ERROR: Expected to create an empty unit context.\n";
    ok = false;
  }
  auto ctxt2 = PreferredUnits::create(sys.get(), false);
  if (!ctxt2 || ctxt2->m_unitsByDimension.empty())
  {
    std::cerr << "ERROR: Expected to create a preconfigured unit context.\n";
    ok = false;
  }
  // Convert the pre-configured unit context into JSON and dump it for
  // inspection and pasting into hand-edited unit system specifications.
  nlohmann::json jj = ctxt2;
  std::cout << jj.dump(2) << "\n";

  // Test that a composite unit properly lists preferred units.
  // We'll use length/time as input dimensions and test that preferred velocity units are listed.
  auto ctxt = sys->activeContext();
  units::CompatibleUnitOptions opts;
  auto suggested = ctxt->suggestedUnits("ft/min", opts);
  // Note the expected result includes not only the suggested units, but the input "ft/min"
  // appended at the end since it is a valid, user-provided alternative.
  std::vector<std::string> expected_m2 = {{"m/s", "km/hr", "mi/hr", "in/s", "ft/s", "ft/min"}};
  std::cout << "Suggested units for \"ft/min\", default options:" << suggested.size() << "\n";
  for (const auto& su : suggested)
  {
    std::cout << "  " << su << "\n";
  }
  ok &= (suggested == expected_m2);

  // Try again with different options:
  std::vector<std::string> expected_m1 = {{"m/s", "km/hr", "mi/hr", "in/s", "ft/s" }};
  opts.m_inputUnitPriority = -1;
  suggested = ctxt->suggestedUnits("ft/min", opts);
  std::cout << "Suggested units for \"ft/min\", -1 user priority:" << suggested.size() << "\n";
  for (const auto& su : suggested)
  {
    std::cout << "  " << su << "\n";
  }
  ok &= (suggested == expected_m1);

  // Test that asterisk returns all units as suggested:
  std::cout << "Suggested units for \"*\":\n";
  suggested = ctxt->suggestedUnits("*", opts);
  for (const auto& su : suggested)
  {
    std::cout << "  " << su << "\n";
  }
  std::vector<std::string> expected_all = {{
    "A", "C", "F", "G", "H", "Hz", "J", "K", "N", "Pa", "T", "V", "V/m", "W", "W/m K", "Wb",
    "atm", "bar", "cd", "ft", "ft/s", "ft/s^2", "hour", "hp", "in", "in/s", "in/s^2",
    "kg", "km/hr", "lbf", "lbm", "m", "m/s", "m/s^2", "mi", "mi/hr", "min", "psi", "rad/s",
    "s", "slug", "yd", "°C", "°F", "°R", "Ω"
  }};
  ok &= (suggested == expected_all);

  // Test that the first preferred unit for each dimension
  // is returned by allPreferredUnits():
  std::cout << "All preferred units:\n";
  suggested = ctxt->allPreferredUnits();
  for (const auto& su : suggested)
  {
    std::cout << "  " << su << "\n";
  }
  std::vector<std::string> expected_preferred = {{
    "A", "C", "F", "H", "Hz", "J", "K", "N", "Pa", "T", "V", "V/m", "W", "W/m K", "Wb",
    "cd", "kg", "m", "m/s", "m/s^2", "s", "Ω"
  }};
  ok &= (suggested == expected_preferred);

  return ok ? 0 : 1;
}
